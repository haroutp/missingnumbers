﻿using System;
using System.Collections.Generic;

namespace MissingNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            

            int[] arr = new int[10]{11, 4, 11, 7, 13, 4, 12, 11, 10, 14};

            int[] brr = new int[15]{11, 4, 11, 7, 3, 7, 10, 13, 4, 8, 12, 11, 10, 14, 12};

            
            
            int[] result = missingNumbers(arr, brr);
            foreach (var item in result)
            {
                System.Console.Write(item + " ");
            }
        }
        static int[] missingNumbers(int[] arr, int[] brr) {
            

            var d1 = new Dictionary<int, int>();
            var d2 = new Dictionary<int, int>();
            
            foreach (var item in arr)
            {
                if(!d1.ContainsKey(item)){
                    d1.Add(item, 1);
                }else{
                    d1[item] += 1;
                }
            }

            foreach (var item in brr)
            {
                if(!d2.ContainsKey(item)){
                    d2.Add(item, 1);
                }else{
                    d2[item] += 1;
                }
            }

            // foreach (var item in d1)
            // {
            //     System.Console.WriteLine(item.Key + ": " + item.Value);
            // }
            // System.Console.WriteLine();
            // foreach (var item in d2)
            // {
            //     System.Console.WriteLine(item.Key + ": " + item.Value);
            // }
            List<int> missingNumbers = new List<int>();
            foreach (var item in d2.Keys)
            {
                if(!d1.ContainsKey(item)){
                    missingNumbers.Add(item);
                }else{
                    if(d2[item] != d1[item]){
                        missingNumbers.Add(item);
                    }
                }
            }
            missingNumbers.Sort();
            int[] resArr = missingNumbers.ToArray();
            return resArr;

        }
    }
}
